<?php

class User_Model extends CI_Model
{
    function checkUserLogin($username, $password) 
    {
        $query = $this->db->select('u_id, u_username')
                ->where('u_username', $username)
                ->where('u_password', md5($password))
                ->where('u_is_deleted', 0)
                ->get('users');

        return ($query->num_rows() === 1) ? $query->row() : NULL;
    }

    function getExistingCV()
    {
        $query = $this->db->select('u_is_cv_uploaded, u_cv_path_url')
                ->where('u_id', $this->session->userdata('u_id'))
                ->get('users');

        return ($query->num_rows() === 1) ? $query->row() : 0;
    }

    function updateCVData($file_name)
    {
        $user_data = array('u_is_cv_uploaded' => 1, 'u_cv_path_url' => $file_name);
        $this->db->where('u_id', $this->session->userdata('u_id'))->update('users', $user_data);
    }
}