<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>User Login</title>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <style>
            body { background-color: #e9ecef; margin: 0; }
            div { display: block; box-sizing: border-box; }
            .login-box { 
                position: absolute;
                top: 50%;
                left: 50%;
                -ms-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
                width: 300px;
            }
            .form-element { width: 100%; margin-bottom: 10px; box-sizing: border-box; }
            label { font-size: 12px; margin-bottom: 0; }
            .login-button { margin-top: 30px; }
            .error { font-size: 12px; color: red; }
            .card { background-color: white; box-shadow: 0 0 6px 0px grey; }
        </style>
    </head>
  
    <body>
        <div class="login-box" style="">
            <h3 style="text-align: center;">User Login</h3>
            <br/>
            <div class="card">
                <div class="card-body">
                    <?php echo form_open('/user/login', array('id' => 'userLogin')); ?>
                    <label for="username">Username:</label>
                    <input class="form-element" type="text" name="username" id="username" placeholder="Username" required="" />
                    <label for="password">Password:</label>
                    <input class="form-element" type="password" name="password" id="password" placeholder="Password" required="" />
                    <input class="form-element btn btn-primary login-button" type="submit" value="Login" name="login-btn" />
                    <?php echo form_close(); ?>
                    <span class="error"><?php echo validation_errors(); ?></span>
                    <?php if($this->session->flashdata('message')) { ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <?php echo $this->session->flashdata('message'); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>