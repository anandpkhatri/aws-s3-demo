-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.38-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_aws_s3
CREATE DATABASE IF NOT EXISTS `db_aws_s3` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `db_aws_s3`;

-- Dumping structure for table db_aws_s3.users
CREATE TABLE IF NOT EXISTS `users` (
  `u_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `u_username` varchar(50) NOT NULL,
  `u_password` varchar(32) NOT NULL,
  `u_is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0 => Active, 1 => Deleted',
  `u_is_cv_uploaded` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `u_cv_path_url` varchar(20) DEFAULT NULL,
  `u_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `u_username` (`u_username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table db_aws_s3.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`u_id`, `u_username`, `u_password`, `u_is_deleted`, `u_is_cv_uploaded`, `u_cv_path_url`, `u_created_at`, `u_modified_at`) VALUES
	(1, 'demo', 'fcea920f7412b5da7be0cf42b8c93759', 0, 0, NULL, '2020-12-07 19:11:17', '2020-12-10 20:03:55');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
